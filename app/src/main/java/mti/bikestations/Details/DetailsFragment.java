package mti.bikestations.Details;

import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import mti.bikestations.Model.StationCtn;
import mti.bikestations.NetworkUtil;
import mti.bikestations.R;

public class DetailsFragment extends Fragment {

    private View view;

    private Toolbar toolbar;

    private TextView nameView;
    private ImageView statusView;
    private TextView bikeStandsView;
    private TextView availableBikeStandsView;
    private TextView addressView;
    private TextView lastUpdateView;
    private StationCtn.Station station;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!NetworkUtil.isNetworkStatusAvailable (getContext()))
            Toast.makeText(getContext(), "Internet is not available.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.view = inflater.inflate(R.layout.details_fragment, container, false);
        this.nameView = (TextView) view.findViewById(R.id.nameDetailsFragment);
        this.statusView = (ImageView) view.findViewById(R.id.statusDetailsFragment);
        this.bikeStandsView = (TextView) view.findViewById(R.id.bikeStandsDetailsFragment);
        this.availableBikeStandsView = (TextView) view.findViewById(R.id.availableBikeStandsDetailsFragment);
        this.addressView = (TextView) view.findViewById(R.id.addressDetailsFragment);
        this.lastUpdateView = (TextView) view.findViewById(R.id.lastUpdateDetailsFragment);

        final StationCtn.Station station = (StationCtn.Station) getArguments().getSerializable("station");

        this.nameView.setText(station.getFields().getName());
        if (station.getFields().getStatus().equals("OPEN"))
            this.statusView.setImageResource(android.R.drawable.presence_online);
        else
            this.statusView.setImageResource(android.R.drawable.presence_invisible);
        this.bikeStandsView.setText(getString(R.string.NbBikeStands, station.getFields().getBikeStands()));
        this.availableBikeStandsView.setText(getString(R.string.NbAvailableBikeStands, station.getFields().getAvailableBikeStands()));
        this.addressView.setText(station.getFields().getAddress());
        this.lastUpdateView.setText(station.getFields().getLastUpdate().toString());
        this.addressView.setTextColor(Color.BLUE);
        this.addressView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double[] coordinates = station.getFields().getPosition();
                Uri localization = Uri.parse(String.format("geo:%1$.10f,%2$.10f?q=", coordinates[0], coordinates[1]) + Uri.encode(station.getFields().getAddress()));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, localization);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });
        return this.view;
    }
}
