package mti.bikestations.Details;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.lang.reflect.Array;
import java.util.ArrayList;

import mti.bikestations.List.StationListActivity;
import mti.bikestations.Model.Singleton;
import mti.bikestations.Model.StationCtn;
import mti.bikestations.Profil.ProfilActivity;
import mti.bikestations.R;

public class StationSlidePagerActivity extends AppCompatActivity {

    private Toolbar toolbar;
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;
    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;
    private ArrayList<StationCtn.Station> stationsList;
    private ShareActionProvider provider;
    private Intent share;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_pager);
        stationsList = getIntent().getParcelableArrayListExtra("stations");
        int position = getIntent().getIntExtra("position", 0);
        // Instantiate a ViewPager and a PagerAdapter
        this.toolbar = (Toolbar)findViewById(R.id.detailToolbar);
        setSupportActionBar(this.toolbar);

        this.mPager = (ViewPager) findViewById(R.id.station_pager);
        this.mPagerAdapter = new StationSlidePagerAdapter(getSupportFragmentManager(), stationsList);
        this.mPager.setAdapter(this.mPagerAdapter);
        this.mPager.setCurrentItem(position);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);

        this.provider = new ShareActionProvider(getBaseContext());

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_share) {
            if (provider != null) {
                StationCtn.Station station = stationsList.get(mPager.getCurrentItem());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, station.getDescription());
                shareIntent.setType("text/plain");
                provider.setShareIntent(shareIntent);
                startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));
            }
            return true;
        }
        if (id == R.id.action_info) {
            Intent intent = new Intent(this, ProfilActivity.class);
            startActivityForResult(intent, 2);
        }

        return super.onOptionsItemSelected(item);
    }
    private class StationSlidePagerAdapter extends FragmentStatePagerAdapter {

        private ArrayList<StationCtn.Station> stationsList;

        public StationSlidePagerAdapter(FragmentManager fm, ArrayList<StationCtn.Station> stations) {
            super(fm);
            this.stationsList = stations;
        }

        @Override
        public Fragment getItem(int position) {

            StationCtn.Station station = stationsList.get(position);
            Bundle bundle = new Bundle();
            bundle.putSerializable("station", station);

            DetailsFragment detailsFragment = new DetailsFragment();
            detailsFragment.setArguments(bundle);

            return detailsFragment;
        }

        @Override
        public int getCount() {
            return stationsList.size();
        }
    }
}
