package mti.bikestations.List;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import mti.bikestations.Details.StationSlidePagerActivity;
import mti.bikestations.Model.StationCtn;
import mti.bikestations.R;


public class StationListAdapter extends RecyclerView.Adapter<StationListAdapter.StationHolder> {

    private ArrayList<StationCtn.Station> stationsList;

    public StationListAdapter(ArrayList<StationCtn.Station> stationsList) {
        this.stationsList = stationsList;
    }

    @Override
    public int getItemCount() {
        return this.stationsList.size();
    }

    @Override
    public StationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.station_recyclerview_item_row,
                parent, false);

        return new StationHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(final StationHolder holder, int position) {
        final StationCtn.Station station = stationsList.get(position);
        holder.position = position;
        holder.nameView.setText(station.getFields().getName());
        if (station.getFields().getStatus().equals("OPEN"))
            holder.statusView.setImageResource(android.R.drawable.presence_online);
        else
            holder.statusView.setImageResource(android.R.drawable.presence_invisible);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), StationSlidePagerActivity.class);
                intent.putParcelableArrayListExtra("stations", stationsList);
                intent.putExtra("position", holder.position);
                v.getContext().startActivity(intent);

            }
        });
    }

    //---------------------------------------------------------------------------------------------

    public static class StationHolder extends RecyclerView.ViewHolder {

        private StationCtn.Station station;
        private TextView nameView;
        private ImageView statusView;
        private int position;

        public StationHolder(View itemView) {
            super(itemView);

            this.nameView = (TextView) itemView.findViewById(R.id.item_station_name);
            this.statusView = (ImageView) itemView.findViewById(R.id.item_station_status);
        }
    }
}
