package mti.bikestations.List;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import mti.bikestations.Model.Singleton;
import mti.bikestations.Model.StationCtn;
import mti.bikestations.NetworkUtil;
import mti.bikestations.Profil.ProfilActivity;
import mti.bikestations.R;

public class StationListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter velibListAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_velib_list);

        if (!NetworkUtil.isNetworkStatusAvailable (getApplicationContext()))
            Toast.makeText(getApplicationContext(), "Internet is not available.", Toast.LENGTH_SHORT).show();

        initView();
    }

    private void initView() {

        this.toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(this.toolbar);

        // Recycler View

        this.recyclerView = (RecyclerView) findViewById(R.id.velib_list_recycler_view);
        this.recyclerView.setHasFixedSize(true);
        this.layoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(this.layoutManager);

        Singleton.getInstance().apply(new Callable<Void>() {

            public Void call() {
                ArrayList<StationCtn.Station> stationsList = Singleton.getInstance().getStationsList();
                velibListAdapter = new StationListAdapter(stationsList);
                recyclerView.setAdapter(velibListAdapter);
                return null;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_list, menu);
//         Retrieve the SearchView and plut it into SearchManager
        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ArrayList<StationCtn.Station> stationsList = Singleton.getInstance().getStationsList();
                ArrayList<StationCtn.Station> filtered = new ArrayList<StationCtn.Station>();
                for (StationCtn.Station station: stationsList) {
                    if (station.getFields().getAddress().toLowerCase().contains(query.toLowerCase())
                            || station.getFields().getName().toLowerCase().contains(query.toLowerCase())) {
                        filtered.add(station);
                    }
                }
                recyclerView.setAdapter(new StationListAdapter(filtered));
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    ArrayList<StationCtn.Station> stationsList = Singleton.getInstance().getStationsList();
                    recyclerView.setAdapter(new StationListAdapter(stationsList));
                }
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        }
        else if (id == R.id.action_info) {
            Intent intent = new Intent(this, ProfilActivity.class);
            startActivityForResult(intent, 2);
        }
        else if (id == R.id.action_refresh) {
            initView();
        }

        return super.onOptionsItemSelected(item);
    }
}
