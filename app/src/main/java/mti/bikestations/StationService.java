package mti.bikestations;

import mti.bikestations.Model.StationCtn;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface StationService {

    public static final String URL = "https://opendata.paris.fr";

    @Headers("Accept: application/vnd.github.v3.full+json")
    @GET("/api/records/1.0/search/?dataset=stations-velib-disponibilites-en-temps-reel&rows=100&facet=banking&facet=bonus&facet=status&facet=contract_name")
    Call<StationCtn> getStations();
}
