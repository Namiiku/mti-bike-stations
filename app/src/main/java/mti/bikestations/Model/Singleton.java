package mti.bikestations.Model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import mti.bikestations.StationService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Singleton {

    private static Singleton instance = null;
    private StationService stationService;

    private StationCtn stationCtn;

    private Singleton() {

        // Initialize retrofit and web service
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(StationService.URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        this.stationService = retrofit.create(StationService.class);
    }

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    /**
     * Set stationCtn by retrieving data from API and apply treatment.
     * We're making sure the treatment function is called after receiving
     * the response.
     */
    private void setStationCtn(final Callable<Void> treatmentFunction) {

        Call<StationCtn> call = stationService.getStations();
        call.enqueue(new Callback<StationCtn>() {
            @Override
            public void onResponse(Call<StationCtn> call, Response<StationCtn> response) {
                if (response.isSuccessful()) {

                    stationCtn = response.body();

                    try {
                        treatmentFunction.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<StationCtn> call, Throwable t) {

                System.out.println("Failure: " + t.getMessage());
            }
        });
    }

    /**
     * Set stationCtn, and apply treatment
     */
    public void apply(Callable<Void> treatmentFunction) {
        setStationCtn(treatmentFunction);
        try {
            treatmentFunction.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<StationCtn.Station> getStationsList() {
        return stationCtn.getStationsList();
    }
}