package mti.bikestations.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class StationCtn implements Serializable {

    @SerializedName("records")
    private final ArrayList<Station> stationsList;

    public StationCtn(ArrayList<Station> stationsList) {
        this.stationsList = stationsList;
    }

    public ArrayList<Station> getStationsList() {
        return stationsList;
    }


    public static class Station implements Serializable, Parcelable {

        @SerializedName("fields")
        private Fields fields;

        public Station(String id, Fields fields) {
            this.fields = fields;
        }

        public Station(Parcel in) {
            fields = in.readParcelable(Fields.class.getClassLoader());
        }

        public static final Creator<Station> CREATOR = new Creator<Station>() {
            @Override
            public Station createFromParcel(Parcel in) {
                return new Station(in);
            }

            @Override
            public Station[] newArray(int size) {
                return new Station[size];
            }
        };

        public Fields getFields() {
            return fields;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(fields, flags);
        }

        public Station readFromParcel(Parcel in) {
            fields = in.readParcelable(Fields.class.getClassLoader());
            return this;
        }

        public String getDescription() {
            return "Name: " + fields.getName() + '\n'
                    + "status: " + fields.getStatus() + '\n'
                    + "bikeStand: " + fields.getBikeStands() + '\n'
                    + "available: " + fields.getAvailableBikeStands() + '\n'
                    + "address: " + fields.getAddress() + '\n';
        }
        public static class Fields implements Serializable, Parcelable {

            private String name;
            private String status; // OPEN / CLOSED
            @SerializedName("bike_stands")
            private int bikeStands;
            @SerializedName("available_bike_stands")
            private int availableBikeStands;
            private String address;
            @SerializedName("last_update")
            private Date lastUpdate;
            private double[] position;

            public Fields(String name, String status, int bikeStands, int availableBikeStands, String address, Date lastUpdate, double[] position) {
                this.name = name;
                this.status = status;
                this.bikeStands = bikeStands;
                this.availableBikeStands = availableBikeStands;
                this.address = address;
                this.lastUpdate = lastUpdate;
                this.position = position;
            }

            protected Fields(Parcel in) {
                name = in.readString();
                status = in.readString();
                bikeStands = in.readInt();
                availableBikeStands = in.readInt();
                address = in.readString();
                lastUpdate = (Date) in.readSerializable();
                position = in.createDoubleArray();
            }

            public static final Creator<Fields> CREATOR = new Creator<Fields>() {
                @Override
                public Fields createFromParcel(Parcel in) {
                    return new Fields(in);
                }

                @Override
                public Fields[] newArray(int size) {
                    return new Fields[size];
                }
            };

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public int getBikeStands() {
                return bikeStands;
            }

            public int getAvailableBikeStands() {
                return availableBikeStands;
            }

            public String getAddress() {
                return address;
            }

            public Date getLastUpdate() {
                return lastUpdate;
            }

            public double[] getPosition() {
                return position;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(name);
                dest.writeString(status);
                dest.writeInt(bikeStands);
                dest.writeInt(availableBikeStands);
                dest.writeString(address);
                dest.writeSerializable(lastUpdate);
                dest.writeDoubleArray(position);
            }

        }
    }
}
